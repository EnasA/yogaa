//
//  secondViewController.swift
//  Yoga
//
//  Created by enas on 7/31/18.
//  Copyright © 2018 socyle. All rights reserved.
//

import UIKit
import AudioToolbox
class secondViewController: UIViewController,CountdownTimerDelegate {
    var numberPoss = ""
    var HoldPoses = ""
    var TransitionTime = ""
    var holdPossMM = ""
    @IBOutlet weak var progressBar: ProgressBar!
    @IBOutlet weak var hours: UILabel!
    @IBOutlet weak var minutes: UILabel!
    @IBOutlet weak var seconds: UILabel!
    @IBOutlet weak var counterView: UIStackView!
    @IBOutlet weak var stopBtn: UIButton!
    @IBOutlet weak var startBtn: UIButton!
    
    @IBOutlet weak var repeatNo: UILabel!
    
    @IBOutlet weak var NoPoses: UILabel!
    
    // Timer 2
    @IBOutlet weak var labelMinute: UILabel!
    @IBOutlet weak var labelSecond: UILabel!
    @IBOutlet weak var labelMillisecond: UILabel!
   // @IBOutlet weak var resetBtn: UIButton!
    
    weak var timer: Timer?
    var startTime: Double = 0
    var time: Double = 0
    var elapsed: Double = 0
    var status: Bool = false
    
    

  
    
    //MARK - Vars
    
    var countdownTimerDidStart = false
    var TimerSelect = false
    lazy var countdownTimer: CountdownTimer = {
        let countdownTimer = CountdownTimer()
        return countdownTimer
    }()
    


    
    // Test, for dev
    var selectedSecs:Int = 0
    let selectHour : Int = 0
    var selecMinutes:Int = 0
    
    lazy var messageLabel: UILabel = {
        let label = UILabel(frame:CGRect.zero)
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont.systemFont(ofSize: 24.0, weight: UIFont.Weight.light)
        label.textColor = UIColor.white
        label.textAlignment = .center
        label.text = "Done!"
        
        return label
    }()


    override func viewDidLoad() {
        super.viewDidLoad()

  SelectTimer()
       
        view.addSubview(messageLabel)
        
        var constraintCenter = NSLayoutConstraint(item: messageLabel, attribute: .centerX, relatedBy: .equal, toItem: self.view, attribute: .centerX, multiplier: 1, constant: 0)
        self.view.addConstraint(constraintCenter)
        constraintCenter = NSLayoutConstraint(item: messageLabel, attribute: .centerY, relatedBy: .equal, toItem: self.view, attribute: .centerY, multiplier: 1, constant: 0)
        self.view.addConstraint(constraintCenter)
        
        messageLabel.isHidden = true
        counterView.isHidden = false

         stopBtn.isEnabled = false        
    }
    
    
    func  SelectTimer(){
        if !TimerSelect
        {        selectedSecs = Int(HoldPoses)! + (Int (TransitionTime)! - 1)
            selecMinutes = Int(holdPossMM)!
            countdownTimer.delegate = self
            countdownTimer.setTimer(hours: selectHour, minutes: selecMinutes, seconds: selectedSecs)
            progressBar.setProgressBar(hours: selectHour, minutes: selecMinutes, seconds: selectedSecs)
            stopBtn.isEnabled = false
            stopBtn.alpha = 0.5
            TimerSelect = false
        }
        else  {
            TimerSelect = false
            selectedSecs = Int (TransitionTime)!
            countdownTimer.delegate = self
            countdownTimer.setTimer(hours: selectHour, minutes: selecMinutes, seconds: selectedSecs)
            progressBar.setProgressBar(hours: selectHour, minutes: selecMinutes, seconds: selectedSecs)
            stopBtn.isEnabled = false
            stopBtn.alpha = 0.5
            
            
            
        }
    }
    
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return .lightContent
    }
    
    
    //MARK: - Countdown Timer Delegate
    
    func countdownTime(time: (hours: String, minutes: String, seconds: String)) {
        hours.text = time.hours
        minutes.text = time.minutes
        seconds.text = time.seconds
    }
    
    
    func countdownTimerDone() {
        
        counterView.isHidden = true
        messageLabel.isHidden = false
        seconds.text = String(selectedSecs)
        minutes.text = String(selecMinutes)
        hours.text = String(selectHour)
        countdownTimerDidStart = false
        stopBtn.isEnabled = false
        stopBtn.alpha = 0.5
        startBtn.setTitle("START",for: .normal)
        //stop timer2
        stop()
        AudioServicesPlayAlertSound(SystemSoundID(kSystemSoundID_Vibrate))
        
        print("countdownTimerDone")
        loadTimer()
     
    }
    
    
    func loadTimer(){
        
        messageLabel.isHidden = true
        counterView.isHidden = false
        
        stopBtn.isEnabled = true
        stopBtn.alpha = 1.0
        if
            count < Int(numberPoss)! {
            start()
            countdownTimer.start()
            progressBar.start()
            countdownTimerDidStart = true
            startBtn.setTitle("PAUSE",for: .normal)
            
            count += 1
            NoPoses.text = numberPoss
            repeatNo.text = String(count)
            
            print(count)
        }
        
        
        
    }
    
    
    
    
    
    //MARK: - Actions
    var count = 0
    

    @IBAction func startTimer(_ sender: UIButton) {
       
        if  !countdownTimerDidStart , !(status){
        DispatchQueue.global(qos: .background).async {
            
            DispatchQueue.main.async {
                self.loadTimer()
            }
        }
        }else{
        stop()
        countdownTimer.pause()
        progressBar.pause()
        countdownTimerDidStart = false
        
        TimerSelect = false
        startBtn.setTitle("RESUME",for: .normal)
        print("ffffffffffffff")
        messageLabel.isHidden = true
        }
    }
    
    
    @IBAction func stopTimer(_ sender: UIButton) {
        countdownTimer.stop()
        progressBar.stop()
        countdownTimerDidStart = false
       // stopBtn.isEnabled = false
        stopBtn.alpha = 0.5
        startBtn.setTitle("START",for: .normal)
     
//Mark: -  Reset Second Timer
        
        // Invalidate timer
        timer?.invalidate()
        
        // Reset timer variables
        startTime = 0
        time = 0
        elapsed = 0
        status = false
        
        // Reset all three labels to 00
        let strReset = String("00")
        labelMinute.text = strReset
        labelSecond.text = strReset
        //  labelMillisecond.text = strReset
        
        
        
        
        
        
    }
  
    
    
    
    //Mark: - Second Timer
    
    
    func start() {
        
        startTime = Date().timeIntervalSinceReferenceDate - elapsed
        timer = Timer.scheduledTimer(timeInterval: 0.01, target: self, selector: #selector(updateCounter), userInfo: nil, repeats: true)
        
        // Set Start/Stop button to true
        status = true
        
    }
    
    func stop() {
        
        elapsed = Date().timeIntervalSinceReferenceDate - startTime
        timer?.invalidate()
        
        // Set Start/Stop button to false
        status = false
        
    }
    
    @objc func updateCounter() {
        
        // Calculate total time since timer started in seconds
        time = Date().timeIntervalSinceReferenceDate - startTime
        
        // Calculate minutes
        let minutes = UInt8(time / 60.0)
        time -= (TimeInterval(minutes) * 60)
        
        // Calculate seconds
        let seconds = UInt8(time)
        time -= TimeInterval(seconds)
        
        // Calculate milliseconds
        // let Hours = UInt8(time * 100)
        
        // Format time vars with leading zero
        let strMinutes = String(format: "%02d", minutes)
        let strSeconds = String(format: "%02d", seconds)
        //  let StrHours = String(format: "%02d", milliseconds)
        
        // Add time vars to relevant labels
        labelMinute.text = strMinutes
        labelSecond.text = strSeconds
        // labelMillisecond.text = StrHours
        
    }
    
    
    
    
    
    @IBAction func Back(_ sender: Any) {
        
        dismiss(animated: true, completion: nil
        )
        
        
    }
    
    
    
    
    
    
}

