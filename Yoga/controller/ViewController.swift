//
//  ViewController.swift
//  Yoga
//
//  Created by enas on 7/31/18.
//  Copyright © 2018 socyle. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

  
    @IBOutlet weak var NoOfPoses: UILabel!
    
    @IBOutlet weak var holdPoses: UILabel!
    
    @IBOutlet weak var lbltransitionTime: UILabel!
    

    @IBOutlet weak var holdPossMM: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    
    }

    @IBAction func numberPoss(_ sender: UIStepper) {
        
        NoOfPoses.text = Int(sender.value).description
    }
    
    @IBAction func HoldPoses(_ sender: UIStepper) {
        
        holdPossMM.text = Int(sender.value).description
    }
    
    
   
    @IBAction func holdPssesMM(_ sender: UIStepper) {
        
        holdPoses.text = Int(sender.value).description
    }
    
    
    
    
    @IBAction func TransitionTime(_ sender: UIStepper) {
        lbltransitionTime.text = Int(sender.value).description
    }
    
    
    @IBAction func Start(_ sender: Any) {
        if self.NoOfPoses.text == "0" , self.holdPoses.text == "SS" , self.lbltransitionTime.text == "SS"  , self.holdPossMM.text == "MM"{
            
            self.showToast(message: "Enter Poss Number")
        
        }else{
            
             performSegue(withIdentifier: "Segue", sender: self)
        }
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let secondView = segue.destination as! secondViewController
        secondView.TransitionTime = lbltransitionTime.text!
        secondView.HoldPoses = holdPoses.text!
        secondView.numberPoss = NoOfPoses.text!
        secondView.holdPossMM = holdPossMM.text!
    }
    
    func showToast(message : String) {
        
        let toastLabel = UILabel(frame: CGRect(x: self.view.frame.size.width/2 - 75, y: self.view.frame.size.height-100, width: 150, height: 35))
        toastLabel.backgroundColor = UIColor.black.withAlphaComponent(0.6)
        toastLabel.textColor = UIColor.white
        toastLabel.textAlignment = .center;
        toastLabel.font = UIFont(name: "Montserrat-Light", size: 12.0)
        toastLabel.text = message
        toastLabel.alpha = 1.0
        toastLabel.layer.cornerRadius = 10;
        toastLabel.clipsToBounds  =  true
        self.view.addSubview(toastLabel)
        UIView.animate(withDuration: 4.0, delay: 0.1, options: .curveEaseOut, animations: {
            toastLabel.alpha = 0.0
        }, completion: {(isCompleted) in
            toastLabel.removeFromSuperview()
        })
    }
    
    
}

