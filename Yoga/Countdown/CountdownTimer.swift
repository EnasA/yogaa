//
//  CountdownTimer.swift
//  Yoga
//
//  Created by enas on 8/1/18.
//  Copyright © 2018 socyle. All rights reserved.
//

import Foundation
import UIKit

protocol CountdownTimerDelegate:class {
    func countdownTimerDone()
    func countdownTime(time: (hours: String, minutes:String, seconds:String))
}

class CountdownTimer {
    
    weak var delegate: CountdownTimerDelegate?
    var selectTimer = true
     var seconds = 0.0
     var duration = 0.0
    
    lazy var timer: Timer = {
         let timer = Timer()
        return timer
    }()
    
    lazy var timer2: Timer = {
        let timer2 = Timer()
        return timer2
    }()
    
    public func setTimer(hours:Int, minutes:Int, seconds:Int) {
        
        let hoursToSeconds = hours * 3600
        let minutesToSeconds = minutes * 60
        let secondsToSeconds = seconds
        
        let seconds = secondsToSeconds + minutesToSeconds + hoursToSeconds
        self.seconds = Double(seconds)
        self.duration = Double(seconds)
        
        delegate?.countdownTime(time: timeString(time: TimeInterval(ceil(duration))))
    }
    
    public func start() {
       
        if selectTimer == true {
           runTimer()
            
        }else{
            
            selectTimer = false
            runTimer2()
        }
        
        
        
    }
    public func pause() {
       
        timer.invalidate()
        timer2.invalidate()
    }
    
    public func stop() {
        timer.invalidate()
        timer2.invalidate()
        duration = seconds
        delegate?.countdownTime(time: timeString(time: TimeInterval(ceil(duration))))
    }
    

     func runTimer() {
     
      
        timer = Timer.scheduledTimer(timeInterval: 0.01, target: self, selector: #selector(updateTimer), userInfo: nil, repeats: true)
      

    
    }
    
    
    func runTimer2() {
        
        
        timer2 = Timer.scheduledTimer(timeInterval: 0.01, target: self, selector: #selector(updateTimer), userInfo: nil, repeats: true)
        
        
        
    }
    
    
    
    @objc fileprivate func updateTimer(){
        
        if duration < 0.0 {
            timer.invalidate()
            timer2.invalidate()
            timerDone()
        } else {
            duration -= 0.01
            delegate?.countdownTime(time: timeString(time: TimeInterval(ceil(duration))))
        }
    }
    
    fileprivate func timeString(time:TimeInterval) -> (hours: String, minutes:String, seconds:String) {
        
        let hours = Int(time) / 3600
        let minutes = Int(time) / 60 % 60
        let seconds = Int(time) % 60
        
        return (hours: String(format:"%02i", hours), minutes: String(format:"%02i", minutes), seconds: String(format:"%02i", seconds))
    }
    
    fileprivate func timerDone() {
        timer.invalidate()
        timer2.invalidate()
        duration = seconds
        delegate?.countdownTimerDone()
}



}



